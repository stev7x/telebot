@extends('main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-dot-circle-o"></i> {{ isset($data['id']) ? "Edit" : "Create" }} Bot User Role
            </h1>
            <br>
            <ol class="breadcrumb">
                <?php $segments = ''; ?>
                @foreach(Request::segments() as $segment)
                    <?php $segments .= '/'.$segment; ?>
                    <li>
                        <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                    </li>
                @endforeach
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-close"></i>Failed</h4>
                                {{ $error }}
                            </div>
                        </div>
                    </div>
            @endforeach
        @endif
        <!-- Content -->
            <div class='row'>
                <div class="center-block col-md-8" style="float: none;">
                    <!-- Horizontal Form -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Form</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="{{ isset($data['id']) ? route('role.update', $data['id']) : route('role.store') }}">
                            @if (isset($data['id']))
                                @method('PATCH')
                            @endif

                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Name</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ @$data['name'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Status</label>

                                    <div class="col-sm-9" style="bottom: -6px;">
                                        <!-- radio -->
                                        <label style="min-width: 130px;">
                                            <input type="radio" name="status" class="minimal" value="1" {{ @$data['status'] == 1 ? 'checked' : '' }}>
                                            Aktif
                                        </label>
                                        <label>
                                            <input type="radio" name="status" class="minimal" value="0" {{ @$data['status'] == 0 ? 'checked' : '' }}>
                                            Non Aktif
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right">{{ isset($data['id']) ? "Save" : "Submit" }}</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>

            </div><!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        // iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-green',
            radioClass   : 'iradio_minimal-green'
        })
    </script>
@endsection