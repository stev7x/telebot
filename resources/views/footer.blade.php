<!-- Main Footer -->
<footer class="main-footer">
    <div class="text-right">
        <strong>Copyright &copy; {{ date("Y") }} <a href="https://www.swamedia.co.id/">Swamedia Informatika</a>.</strong>
    </div>
</footer>

</div>
<!-- ./wrapper -->

<!-- Global Script -->
<script>
    $( document ).ready(function() {
        //Initialize Select2 Elements
        $('.select2').select2()

        // datatable
        $('#data').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
        });
    });
</script>

</html>