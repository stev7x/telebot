<!-- Header -->
@include('header')

<!-- Sidebar -->
@include('sidebar')

<!-- Content -->
@yield('content')

<!-- Footer -->
@include('footer')