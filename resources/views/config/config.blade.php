@extends('main')

@section('content')
    <style>
        .form-horizontal .control-label.text-left{ text-align: left; }
        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
        .toggle.ios .toggle-handle { border-radius: 20rem; }
    </style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="icon fa fa-toggle-on"></i> Bot Configuration
            </h1>
            <br>
            <ol class="breadcrumb">
                <?php $segments = ''; ?>
                @foreach(Request::segments() as $segment)
                    <?php $segments .= '/'.$segment; ?>
                    <li>
                        <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                    </li>
                @endforeach
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">

            @if(session()->get('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i>Success</h4>
                            {{ session()->get('success') }}
                        </div>
                    </div>
                </div>
            @endif

            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-close"></i>Failed</h4>
                                {{ $error }}
                            </div>
                        </div>
                    </div>
            @endforeach
        @endif

        <!-- Content -->
            <div class='row'>
                <div class="center-block col-md-8" style="float: none;">
                    <!-- Horizontal Form -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Configuration</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="token_bot" class="col-sm-3 control-label">Bot Token</label>

                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input id="token_bot" type="token_bot" value="{{ $token }}" class="form-control" disabled>

                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">Change</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bot_activate" class="col-sm-3 control-label">Bot</label>

                                    <div class="col-sm-9">
                                        <input id="bot_activate" type="checkbox" {{ $status == 1 ? "checked" : "" }} data-toggle="toggle" data-width="75" data-style="ios" data-onstyle="success">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- /.row -->

                <div class="modal fade" id="modal-default">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label" style="top: 5px;">New Token</label>

                                        <div class="col-sm-9">
                                            <input id="new_token_bot" type="text" value="{{ $token }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="input-group-btn">
                                    <button id="save_token" type="button" class="btn btn-success">Save</button>
                                </div>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        $(function() {
            // Here you register for the event and do whatever you need to do.
            $(document).on('data-attribute-changed', function() {
                var data = $('#contains-data').data('mydata');
                alert('Data changed to: ' + data);
            });

            $('#button').click(function() {
                $('#contains-data').data('mydata', 'foo');
                // Whenever you change the attribute you will user the .trigger
                // method. The name of the event is arbitrary
                $(document).trigger('data-attribute-changed');
            });

            $('#getbutton').click(function() {
                var data = $('#contains-data').data('mydata');
                alert('Data is: ' + data);
            });

            $('#bot_activate').change(function() {
                if ($(this).is(":checked")) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('bot_activate') }}" + "?status=1",
                        success: function(data) {
                            alert("Webhook now Active");
                        },
                    });
                } else {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('bot_activate') }}" + "?status=0",
                        success: function(data) {
                            alert(data);
                        },
                    });
                }
            });

            $('#save_token').click(function() {
                 var new_token = $('#new_token_bot').val();
                 var old_token = $('#token_bot').val();

                if (new_token != old_token) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('change_token') }}" + "?new_token=" + new_token,
                        success: function(data) {
                            $('#modal-default').modal('toggle');
                            console.log('change token ' + data);
                            $('#token_bot').val(new_token);
                        },
                    });
                } else {
                    alert("That is the same token");
                }
            });
        });
    </script>
@endsection