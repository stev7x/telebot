@extends('main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-dot-circle-o"></i> {{ isset($data['id']) ? "Edit" : "Create" }} Bot User
            </h1>
            <br>
            <ol class="breadcrumb">
                <?php $segments = ''; ?>
                @foreach(Request::segments() as $segment)
                    <?php $segments .= '/'.$segment; ?>
                    <li>
                        <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                    </li>
                @endforeach
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-close"></i>Failed</h4>
                                {{ $error }}
                            </div>
                        </div>
                    </div>
            @endforeach
        @endif
        <!-- Content -->
            <div class='row'>
                <div class="center-block col-md-8" style="float: none;">
                    <!-- Horizontal Form -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Form</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="{{ isset($data['id']) ? route('botuser.update', $data['id']) : route('botuser.store') }}">
                            @if (isset($data['id']))
                                @method('PATCH')
                            @endif

                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Name</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ @$data['name'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="username" class="col-sm-3 control-label">Username</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{ @$data['username'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-3 control-label">Password</label>

                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password" class="col-sm-3 control-label">Confirm Password</label>

                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password">
                                        @if (isset($data['id']))
                                            <p class="help-block"><i>if you don't want to change the password, ignore the input <b>password</b> and input <b>confirm password</b></i></p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Role</label>

                                    <div class="col-sm-9" style="bottom: -6px;">
                                        <!-- checkbox -->
                                        @if($role['countData'] != 0)
                                            @foreach($role['data'] as $key => $value)
                                                <label>
                                                    <input type="radio" name="role_id" class="minimal" value="{{ $value['id'] }}" {{ @in_array($value['id'], $data['role_id']) ? 'checked' : '' }}>
                                                    {{ $value['name'] }}
                                                </label>
                                                <br>
                                            @endforeach
                                        @else
                                            <div class="input-group">
                                                <p>Data role is empty, please add data role first.</p>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                @if (isset($data['id']))
                                    <div class="form-group">
                                        <label for="status" class="col-sm-3 control-label">Status</label>

                                        <div class="col-sm-9" style="bottom: -6px;">
                                            <!-- radio -->
                                            <label style="min-width: 130px;">
                                                <input type="radio" name="status" class="minimal" value="1" {{ @$data['status'] == 1 ? 'checked' : '' }}>
                                                Aktif
                                            </label>
                                            <label>
                                                <input type="radio" name="status" class="minimal" value="0" {{ @$data['status'] == 0 ? 'checked' : '' }}>
                                                Non Aktif
                                            </label>
                                        </div>
                                    </div>
                                @endif
                                {{--<hr>--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="telegram_id" class="col-sm-3 control-label">Telegram ID</label>--}}

                                    {{--<div class="col-sm-9">--}}
                                        <input type="hidden" class="form-control" id="telegram_id" name="telegram_id" placeholder="Telegram ID" value="{{ @$data['telegram_id'] }}">
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right">{{ isset($data['id']) ? "Save" : "Submit" }}</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>

            </div><!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        // iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-green',
            radioClass   : 'iradio_minimal-green'
        })

        $(function () {
            var checkAll = $('input.check_all');
            var checkboxes = $('input.check');

            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-green',
                radioClass   : 'iradio_minimal-green'
            });

            if(checkboxes.filter(':checked').length == checkboxes.length) {
                checkAll.prop('checked', true);
            } else {
                checkAll.prop('checked', false);
            }

            checkAll.iCheck('update');

            checkAll.on('ifChecked ifUnchecked', function(event) {
                if (event.type == 'ifChecked') {
                    checkboxes.iCheck('check');
                } else {
                    checkboxes.iCheck('uncheck');
                }
            });

            checkboxes.on('ifChanged', function(event){
                if(checkboxes.filter(':checked').length == checkboxes.length) {
                    checkAll.prop('checked', true);
                } else {
                    checkAll.prop('checked', false);
                }

                checkAll.iCheck('update');
            });
        });
    </script>
@endsection