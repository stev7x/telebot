@extends('main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-dot-circle-o"></i> {{ isset($data['id']) ? "Edit" : "Create" }} Cabang
            </h1>
            <br>
            <ol class="breadcrumb">
                <?php $segments = ''; ?>
                @foreach(Request::segments() as $segment)
                    <?php $segments .= '/'.$segment; ?>
                    <li>
                        <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                    </li>
                @endforeach
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-close"></i>Failed</h4>
                                {{ $error }}
                            </div>
                        </div>
                    </div>
            @endforeach
        @endif
        <!-- Content -->
            <div class='row'>
                <div class="center-block col-md-8" style="float: none;">
                    <!-- Horizontal Form -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Form</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="{{ isset($data['id']) ? route('cabang.update', $data['id']) : route('cabang.store') }}">
                            @if (isset($data['id']))
                                @method('PATCH')
                            @endif

                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Name</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ @$data['nama_cabang'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="host" class="col-sm-3 control-label">Host</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="host" name="host" placeholder="Host" value="{{ @$data['host'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="port" class="col-sm-3 control-label">Port</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="port" name="port" placeholder="Port" value="{{ @$data['port'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sid" class="col-sm-3 control-label">SID</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="sid" name="sid" placeholder="SID" value="{{ @$data['sid'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="username" class="col-sm-3 control-label">Username</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{ @$data['username'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-3 control-label">Password</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="password" name="password" placeholder="Password" value="{{ @$data['password'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Status</label>

                                    <div class="col-sm-9" style="bottom: -6px;">
                                        <!-- radio -->
                                        <label style="min-width: 130px;">
                                            <input type="radio" name="status" class="minimal" value="1" {{ @$data['status'] == 1 ? 'checked' : '' }}>
                                            Aktif
                                        </label>
                                        <label>
                                            <input type="radio" name="status" class="minimal" value="0" {{ @$data['status'] == 0 ? 'checked' : '' }}>
                                            Non Aktif
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right">{{ isset($data['id']) ? "Save" : "Submit" }}</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>

            </div><!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>

    </script>
@endsection