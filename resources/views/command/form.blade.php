@extends('main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-dot-circle-o"></i> {{ isset($data['id']) ? "Edit" : "Create" }} Command
            </h1>
            <br>
            <ol class="breadcrumb">
                <?php $segments = ''; ?>
                @foreach(Request::segments() as $segment)
                    <?php $segments .= '/'.$segment; ?>
                    <li>
                        <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                    </li>
                @endforeach
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-close"></i>Failed</h4>
                                {{ $error }}
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <!-- Content -->
            <div class='row'>
                <div class="center-block col-md-8" style="float: none;">
                    <!-- Horizontal Form -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Form</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="{{ isset($data['id']) ? route('command.update', $data['id']) : route('command.store') }}">
                            @if (isset($data['id']))
                                @method('PATCH')
                            @endif

                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Name</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ @$data['name'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-3 control-label">Desciption</label>

                                    <div class="col-sm-9">
                                        <textarea id="description" name="description" class="form-control" rows="3" placeholder="Description">{{ @$data['description'] }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="command" class="col-sm-3 control-label">Head Command</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="command" name="command" placeholder="Head Command" value="{{ @$data['command'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="paramater" class="col-sm-3 control-label">Body Command</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="param" name="param" placeholder="Body Command" value="{{ @$data['param'] }}">
                                        <p class="help-block"><i>use <b>hashtag(#)</b>  for separator between paramaters</i></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="module" class="col-sm-3 control-label">Module</label>

                                    <!-- radio -->
                                    <div class="col-sm-9" style="bottom: -5px;">
                                        <label style="min-width: 130px;">
                                            <input type="radio" name="internal_module" class="minimal" value="1" {{ @$data['internal_module'] == 1 ? 'checked' : '' }}>
                                            Internal
                                        </label>
                                        <label>
                                            <input type="radio" name="internal_module" class="minimal" value="0" {{ @$data['internal_module'] == 0 ? 'checked' : '' }}>
                                            External
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="urlModule" class="col-sm-3 control-label"></label>

                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="moduleType">{{ @$data['internal_module'] ? 'URL' : @$data['internal_module'] == 1 ? 'Route' : 'URL' }}</span>
                                            <input type="text" id="module" name="module" class="form-control" value="{{ @$data['module'] }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="method" class="col-sm-3 control-label">Method</label>

                                    <div class="col-sm-9">
                                        <select class="form-control" name="method" >
                                            <option value="GET" {{ @$data['method'] == "GET" ? 'selected' : '' }}>GET</option>
                                            <option value="POST" {{ @$data['method'] == "POST" ? 'selected' : '' }}>POST</option>
                                            <option value="PUT" {{ @$data['method'] == "PUT" ? 'selected' : '' }}>PUT</option>
                                            <option value="PATCH" {{ @$data['method'] == "PATCH" ? 'selected' : '' }}>PATCH</option>
                                            <option value="DELETE" {{ @$data['method'] == "DELETE" ? 'selected' : '' }}>DELETE</option>
                                            <option value="OPTION" {{ @$data['method'] == "OPTION" ? 'selected' : '' }}>OPTION</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Role</label>

                                    <div class="col-sm-9" style="bottom: -6px;">
                                        <!-- checkbox -->
                                        @if($role['countData'] != 0)
                                            <label>
                                                <input class="check_all minimal" type="checkbox">
                                                <i>All</i>
                                            </label>
                                            <br>

                                            @foreach($role['data'] as $key => $value)
                                                <label>
                                                    <input type="checkbox" class="check minimal" name="role_id[]" value="{{ $value['id'] }}" {{ @in_array($value['id'], $data['role_id']) ? 'checked' : '' }}>
                                                    {{ $value['name'] }}
                                                </label>
                                                <br>
                                            @endforeach
                                        @else
                                            <div class="input-group">
                                                <p>Data role is empty, please add data role first.</p>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Status</label>

                                    <div class="col-sm-9" style="bottom: -6px;">
                                        <!-- radio -->
                                        <label style="min-width: 130px;">
                                            <input type="radio" name="status" class="minimal" value="1" {{ @$data['status'] == 1 ? 'checked' : '' }}>
                                            Aktif
                                        </label>
                                        <label>
                                            <input type="radio" name="status" class="minimal" value="0" {{ @$data['status'] == 0 ? 'checked' : '' }}>
                                            Non Aktif
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right">{{ isset($data['id']) ? "Save" : "Submit" }}</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>

            </div><!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        $('input[name="internal_module"]').on('ifClicked', function (e) {
            if (this.value == 1) {
                $("#moduleType").text("Route");
            } else {
                $("#moduleType").text("URL");
            }
        });

        $(function () {
            var checkAll = $('input.check_all');
            var checkboxes = $('input.check');

            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-green',
                radioClass   : 'iradio_minimal-green'
            });

            if(checkboxes.filter(':checked').length == checkboxes.length) {
                checkAll.prop('checked', true);
            } else {
                checkAll.prop('checked', false);
            }

            checkAll.iCheck('update');

            checkAll.on('ifChecked ifUnchecked', function(event) {
                if (event.type == 'ifChecked') {
                    checkboxes.iCheck('check');
                } else {
                    checkboxes.iCheck('uncheck');
                }
            });

            checkboxes.on('ifChanged', function(event){
                if(checkboxes.filter(':checked').length == checkboxes.length) {
                    checkAll.prop('checked', true);
                } else {
                    checkAll.prop('checked', false);
                }

                checkAll.iCheck('update');
            });
        });
    </script>
@endsection