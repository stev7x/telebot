@extends('main')

@section('content')
    <style>
        .deleteForm {
            all: unset;
        }

        .example-modal .modal {
            position: relative;
            top: auto;
            bottom: auto;
            right: auto;
            left: auto;
            display: block;
            z-index: 1;
        }

        .example-modal .modal {
            background: transparent !important;
        }
    </style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-dot-circle-o"></i> Panel User
            </h1>
            <br>
            <ol class="breadcrumb">
                <?php $segments = ''; ?>
                @foreach(Request::segments() as $segment)
                    <?php $segments .= '/'.$segment; ?>
                    <li>
                        <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                    </li>
                @endforeach
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">

            @if(session()->get('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i>Success</h4>
                            {{ session()->get('success') }}
                        </div>
                    </div>
                </div>
            @endif

            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-close"></i>Failed</h4>
                                {{ $error }}
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

            <!-- Content -->
            <div class='row'>
                <div class='col-md-12'>
                    <!-- Box -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <div class="col-md-10">

                            </div>
                            <div class="col-md-2">
                                <a href="{{ route('panel.create') }}" class="btn btn-block btn-success"><i class="fa fa-plus"></i> Add Data</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <table id="data" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    {{--<th>Status</th>--}}
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--Data--}}
                                <?php $i = 1 ?>
                                @foreach($list as $key => $data)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $data['name'] }}</td>
                                        <td>{{ $data['email'] }}</td>
                                        {{--<td class="text-center"><span class="label bg-{{ $data['status']==1? 'green' : 'red' }}">{{ $data['status']==1? 'Active' : 'Non Active' }}</span></td>--}}
                                        <td class="text-center">
                                            <button class="btn btn-sm btn-info detailPage" data-toggle="modal" data-target="#modal-default" data_id="{{ $data['id'] }}"><i class="fa fa-info-circle"></i></button>
                                            <a href="{{ route('panel.edit', $data['id']) }}" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o"></i></a>
                                            <form class="deleteForm" action="{{ route('panel.destroy', $data['id']) }}" row="{{ $data['name'] }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-sm btn-danger" id="btn-form"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                {{--End Data--}}
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->

                    </div><!-- /.box -->
                </div><!-- /.col -->

            </div><!-- /.row -->

            <div class="modal fade" id="modal-default">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Name</label>

                                    <div class="col-sm-9">
                                        <p id="name" class="control-label">Name</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label">email</label>

                                    <div class="col-sm-9">
                                        <p id="email" class="control-label">email</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        jQuery(document).ready(function(){
            jQuery('.detailPage').click(function(e){
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ url('panel') }}" + "/" + $(this).attr('data_id'),
                    method: 'GET',
                    success: function(result) {
                        console.log(result);
                        $('.modal-title').text("Detail Panel User");
                        $('#name').text(result.data.name ? result.data.name : '-');
                        $('#email').text(result.data.email ? result.data.email : '-');
                    }
                });
            });
        });

        $(".deleteForm").on("submit", function(){
            return confirm("Are you sure permanently delete " + this.getAttribute("row") + " ?");
        });
    </script>
@endsection