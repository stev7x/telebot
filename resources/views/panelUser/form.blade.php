@extends('main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="fa fa-dot-circle-o"></i> {{ isset($data['id']) ? "Edit" : "Create" }} Panel User
            </h1>
            <br>
            <ol class="breadcrumb">
                <?php $segments = ''; ?>
                @foreach(Request::segments() as $segment)
                    <?php $segments .= '/'.$segment; ?>
                    <li>
                        <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                    </li>
                @endforeach
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-close"></i>Failed</h4>
                                {{ $error }}
                            </div>
                        </div>
                    </div>
            @endforeach
        @endif
        <!-- Content -->
            <div class='row'>
                <div class="center-block col-md-8" style="float: none;">
                    <!-- Horizontal Form -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Form</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="{{ isset($data['id']) ? route('panel.update', $data['id']) : route('panel.store') }}">
                            @if (isset($data['id']))
                                @method('PATCH')
                            @endif

                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Name</label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ @$data['name'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label">Email</label>

                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ @$data['email'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-3 control-label">Password</label>

                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password" class="col-sm-3 control-label">Confirm Password</label>

                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right">{{ isset($data['id']) ? "Save" : "Submit" }}</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>

            </div><!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection