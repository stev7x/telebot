<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="{{ (request()->is('home*')) ? 'active' : '' }}">
                <a href="{{ route('home') }}">
                    <i class="fa fa-home"></i> <span>Home</span>
                </a>
            </li>
            <li class="{{ (request()->is('bot/*')) ? 'active' : '' }}">
                <a href="{{ route('home') }}">
                    <i class="fa fa-telegram"></i> <span>Bot</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder-o"></i> <span>Master</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ (request()->is('command*')) ? 'active' : '' }}">
                        <a href="{{ route('command.index') }}">
                            <i class="fa fa-{{ (request()->is('command*')) ? 'dot-' : '' }}circle-o"></i> Command
                        </a>
                    </li>
                    <li class="{{ (request()->is('role*')) ? 'active' : '' }}">
                        <a href="{{ route('role.index') }}">
                            <i class="fa fa-{{ (request()->is('role*')) ? 'dot-' : '' }}circle-o"></i> Role
                        </a>
                    </li>
                    <li class="{{ (request()->is('botuser*')) ? 'active' : '' }}">
                        <a href="{{ route('botuser.index') }}">
                            <i class="fa fa-{{ (request()->is('botuser*')) ? 'dot-' : '' }}circle-o"></i> User Bot
                        </a>
                    </li>
                    <li class="{{ (request()->is('panel*')) ? 'active' : '' }}">
                        <a href="{{ route('panel.index') }}">
                            <i class="fa fa-{{ (request()->is('panel*')) ? 'dot-' : '' }}circle-o"></i> User Panel
                        </a>
                    </li>
                    <li class="{{ (request()->is('cabang*')) ? 'active' : '' }}">
                        <a href="{{ route('cabang.index') }}">
                            <i class="fa fa-{{ (request()->is('cabang*')) ? 'dot-' : '' }}circle-o"></i> Cabang
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ (request()->is('log*')) ? 'active' : '' }}">
                <a href="{{ route('log.index') }}">
                    <i class="fa fa-clipboard"></i> <span>Log</span>
                </a>
            </li>
            <li class="{{ (request()->is('telegramconfig*')) ? 'active' : '' }}">
                <a href="{{ route('telegramconfig.index') }}">
                    <i class="fa fa-toggle-on"></i> <span>Config</span>
                </a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

<script>
    // for treeview
    if ($('ul.treeview-menu li').hasClass("active")) {
        $('ul.treeview-menu').parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
    }
</script>