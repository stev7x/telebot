@extends('main')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <i class="icon fa fa-clipboard"></i> User Bot Log
            </h1>
            <br>
            <ol class="breadcrumb">
                <?php $segments = ''; ?>
                @foreach(Request::segments() as $segment)
                    <?php $segments .= '/'.$segment; ?>
                    <li>
                        <a href="{{ $segments }}"><i>{{ ucfirst($segment) }}</i></a>
                    </li>
                @endforeach
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">

            @if(session()->get('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i>Success</h4>
                            {{ session()->get('success') }}
                        </div>
                    </div>
                </div>
            @endif

            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-close"></i>Failed</h4>
                                {{ $error }}
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

            <!-- Content -->
            <div class='row'>
                <div class='col-md-12'>
                    <div class="box box-default">
                        <div class="box-body">
                            <table id="data" class="table table-bordered table-striped">
                                <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Date and Time</th>
                                    <th>Telegram ID</th>
                                    <th>Bot User</th>
                                    <th>Activity</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--Data--}}
                                <?php $i = 1 ?>
                                @foreach($list as $key => $data)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $data['created_at'] }}</td>
                                        <td>{{ $data['telegram_id'] }}</td>
                                        <td>{{ isset($botuser[$data['bot_user_id']]['name']) ? ($data['bot_user_id'] != 0 ? $botuser[$data['bot_user_id']]['name'] : "Not Login") : "*Deleted user" }}</td>
                                        <td>{{ $data['activity'] }}</td>
                                        <td class="text-center"><span class="label bg-{{ $data['success']==1? 'green' : 'red' }}">{{ $data['success']==1? 'SUCCESS' : 'FAIL' }}</span></td>
                                    </tr>
                                @endforeach
                                {{--End Data--}}
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->

                    </div><!-- /.box -->
                </div><!-- /.col -->

            </div><!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection