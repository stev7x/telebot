<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('bot')->group(function () {
    Route::post('register', 'Telegram\AuthController@register')->name('api.bot.register');
    Route::post('login', 'Telegram\AuthController@login')->name('api.bot.login');
    Route::post('logout', 'Telegram\AuthController@logout')->name('api.bot.logout');

    Route::get('commandlist', 'Telegram\CommandController@commandList')->name('api.bot.commandlist');
    Route::get('cabanglist', 'Telegram\TestController@cabangList')->name('api.bot.cabanglist');
    Route::get('test', 'Telegram\TestController@cabang')->name('api.bot.test');
});