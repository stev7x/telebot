<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    // Authentication Routes...
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('/command', 'CommandController');
    Route::resource('/cabang', 'CabangController');
    Route::resource('/panel', 'PanelUserController');
    Route::resource('/botuser', 'BotUserController');
    Route::resource('/log', 'LogController');
    Route::resource('/role', 'RoleController');
    Route::resource('/telegramconfig', 'TelegramConfigController');
    Route::get('/bot_activate', 'TelegramConfigController@bot_activate');
    Route::get('/change_token', 'TelegramConfigController@change_token');
});

Route::match(['get', 'post'], '/core', 'Telegram\CoreController@handle')->name('core');
Route::match(['get', 'post'], '/coreapi', 'Telegram\CoreController@coreAPI')->name('coreapi');

// file handle
Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('app/output/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});