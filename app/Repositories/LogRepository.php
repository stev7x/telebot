<?php

namespace App\Repositories;

use App\Interfaces\LogInterface;
use Illuminate\Database\Eloquent\Model;

class LogRepository implements LogInterface
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
        return $this->model->all();
    }

    // create a new record in the database
    public function create($created_by, $created_at, $telegram_id, $bot_user_id, $activity, $success)
    {
        $data['created_by'] = $created_by;
        $data['created_at'] = $created_at;
        $data['telegram_id'] = $telegram_id;
        $data['bot_user_id'] = $bot_user_id;
        $data['activity'] = $activity;
        $data['success'] = $success;

        return $this->model->create($data);
    }

    // update record in the database
    public function update(array $data, $id)
    {
        $record = $this->find($id);
        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function where($param)
    {
        return $this->model->findByName($param);
    }
}