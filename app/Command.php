<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    // connection
    protected $connection = 'mysql';
    // table name
    protected $table = 'command';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'updated_by',
        'updated_at',
        'created_by',
        'created_at',
        'name',
        'description',
        'command',
        'param',
        'params_number',
        'internal_module',
        'module',
        'method',
        'role_id',
        'status'
    ];

    public function findByCommand($command)
    {
        return self::where('command', $command)->first();
    }
}
