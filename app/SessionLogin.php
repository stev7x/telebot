<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionLogin extends Model
{
    // connection
    protected $connection = 'mysql';
    // table name
    protected $table = 'session_login';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'created_by',
        'updated_by',
        'bot_user_id',
        'telegram_id',
        'start',
        'end',
        'status'
    ];
}
