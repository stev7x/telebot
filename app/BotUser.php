<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BotUser extends Model
{
    // connection
    protected $connection = 'mysql';
    // table name
    protected $table = 'bot_user';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'created_by', 'updated_by', 'name', 'username', 'password', 'telegram_id', 'role_id', 'status'
    ];
}
