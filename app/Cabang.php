<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
//    protected $connection = 'dummy';
    protected $connection = 'mysql';

    protected $table = 'data_cabang';

    public $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'created_by',
        'updated_by',
        'nama_cabang',
        'host',
        'port',
        'sid',
        'username',
        'password',
        'status'
    ];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
}
