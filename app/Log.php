<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    // connection
    protected $connection = 'mysql';
    // table name
    protected $table = 'log';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'created_by',
        'created_at',
        'telegram_id',
        'bot_user_id',
        'activity',
        'success',
    ];
}
