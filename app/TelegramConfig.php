<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelegramConfig extends Model
{
    // connection
    protected $connection = 'mysql';
    // table name
    protected $table = 'telegram_config';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'created_by',
        'updated_by',
        'name',
        'value'
    ];
}
