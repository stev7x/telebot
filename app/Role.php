<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // connection
    protected $connection = 'mysql';
    // table name
    protected $table = 'role';
    // primary key
    public $primaryKey = 'id';
    // timestamps
    public $timestamps = true;

    protected $fillable = [
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'name',
        'status'
    ];
}
