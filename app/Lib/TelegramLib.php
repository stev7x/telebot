<?php

namespace App\Lib;

use App\Repositories\TelegramConfigRepository;
use App\TelegramConfig;
use GuzzleHttp\Client;
use Telegram\Bot\Api;

class TelegramLib
{
    protected $config, $token, $tokenId, $telegramUrl, $telegrambot, $client;

    public function __construct()
    {
        $this->config       = new TelegramConfigRepository(new TelegramConfig());
        $this->token        = empty($this->config->all()->firstWhere('name', 'token_bot')['value']) ? env('TELEGRAM_BOT_TOKEN') : $this->config->all()->firstWhere('name', 'token_bot')['value'];
        $this->tokenId      = $this->config->all()->firstWhere('name', 'token_bot')['id'];
        $this->telegramUrl  = $this->config->all()->firstWhere('name', 'telegram_bot_url')['value'] . $this->token;
        $this->telegrambot  = new Api($this->token);
        $this->client       = new Client();
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $data['value'] = $token;

        return $this->config->update($data, $this->tokenId);
    }

    public function resetToken()
    {
        $data['value'] = '';

        return $this->config->update($data, $this->tokenId);
    }

    public function setWebhook()
    {
        $data['url'] = url("/core");

        return $this->telegrambot->setWebhook($data);
    }

    public function deteleWebhook()
    {
        return $this->telegrambot->removeWebhook();
    }

    public function getWebhookInfo()
    {
        $response = $this->client->request("POST",$this->telegramUrl . "/getWebhookInfo");

        return $response->getBody();
    }

    public function bot()
    {
        return $this->telegrambot;
    }

    public function sendMessage($chat_id, $message)
    {
        $this->telegrambot->sendMessage([
            'chat_id' => $chat_id,
            'text' => $message
        ]);
    }

    public function sendDocument($chat_id, $document, $caption)
    {
        $this->telegrambot->sendDocument([
            'chat_id' => $chat_id,
            'document' => $document,
            'caption' => $caption
        ]);
    }
}
