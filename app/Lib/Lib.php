<?php

namespace App\Lib;

class Lib
{
    public function response($success, $message, $return_file, $file_url, $http_response)
    {
        return response()->json([
            'success' => $success,
            'message' => $message,
            'return_type' => $return_file,
            'file_url' => $file_url
        ], $http_response);
    }

    public function dateNow() {
        return date('Y-m-d H:i:s');
    }
}
