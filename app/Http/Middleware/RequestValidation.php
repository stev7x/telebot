<?php

namespace App\Http\Middleware;

use Closure;

class RequestValidation
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
