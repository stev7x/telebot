<?php

namespace App\Http\Controllers\Telegram;

use App\Command;
use App\Lib\Lib;
use App\Lib\TelegramLib;
use App\Repositories\CommandRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommandController extends Controller
{
    protected $comm, $bot, $lib;

    public function __construct(Command $comm)
    {
        $this->comm = new CommandRepository($comm);
        $this->bot  = new TelegramLib();
        $this->lib  = new Lib();
    }

    public function commandList(Request $request)
    {
        try
        {
            $all        = isset($request->all()['data_valid']) ? json_decode($request->all()['data_valid'], true) : json_decode($request->getContent(), true);
            $request_by = $all['request_by'];
            $command    = $this->comm->all()->where('status', 1);

            if($command->count() != 0)
            {
                $message = "----- List Available Command -----\n";

                foreach ($command as $key => $value)
                {
                    $message = $message . "\n- " . ucfirst($value['name']);
                    $message = $message . "\n  Description       : " . $value['description'];
                    $message = $message . "\n  Command           : " . $value['command'];
                    $message = $message . "\n  Body Command      : " . str_replace("command", $value['command'], $value['param']);;
                    $message = $message . "\n  Parameter numbers : " . $value['params_number'];
                    $message = $message . "\n";
                }
            }
            else
            {
                $message  = "There's no command available right now.";
            }

            $response = $this->lib->response(TRUE, $message, FALSE, FALSE, 200);
            $this->bot->sendMessage($request_by, $message);

            return $response;
        }
        catch (\Exception $e)
        {
            return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 500);
        }
    }
}
