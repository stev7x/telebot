<?php

namespace App\Http\Controllers\Telegram;

use App\BotUser;
use App\Command;
use App\Http\Controllers\Controller;
use App\Lib\Lib;
use App\Lib\TelegramLib;
use App\Log;
use App\Repositories\BotUserRepository;
use App\Repositories\CommandRepository;
use App\Repositories\LogRepository;
use App\Repositories\SessionLoginRepository;
use App\SessionLogin;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class CoreController extends Controller
{
    protected $command, $botuser, $client, $log, $lib, $sessionLogin, $telegram;

    public function __construct(Command $command, BotUser $botUser, Log $log, SessionLogin $sessionLogin)
    {
        $this->telegram     = new TelegramLib();
        $this->client       = new Client();
        $this->lib          = new Lib();
        $this->command      = new CommandRepository($command);
        $this->botuser      = new BotUserRepository($botUser);
        $this->sessionLogin = new SessionLoginRepository($sessionLogin);
        $this->log          = new LogRepository($log);
    }

    public function handle(Request $request)
    {
        $updates = $this->telegram->bot()->getWebhookUpdates();

        if (isset($updates['message']['text']))
        {
            $request_bot   = $updates['message']['text'];
            $chat_id       = $updates['message']['from']['id'];
            $params        = explode('#', $request_bot);
            $commandDetail = $this->command->where($params[0]);
            $dataLogged    = $this->sessionLogin->all()->where('telegram_id', $chat_id)->where('status', 1)->last();
            $bot_response  = ["Ok got it! please wait", "I got it", "okee, please wait", "ok got it", "please wait"];
            $success       = 1;

            if ($commandDetail)
            {
                if ($commandDetail['status'] == 0)
                {
                    $this->telegram->sendMessage($chat_id, "Command non aktif!");
                    goto log;
                }

                if (sizeof($params) > $commandDetail['params_number'])
                {
                    $this->telegram->sendMessage($chat_id, "Parameter lebih!");
                    goto log;
                }

                if (sizeof($params) < $commandDetail['params_number']) {
                    $this->telegram->sendMessage($chat_id, "Parameter kurang!");
                    goto log;
                }

                $this->telegram->sendMessage($chat_id, $bot_response[rand(0, 4)]);

                $key                = explode("#", $commandDetail['param']);
                $data['request_by'] = $chat_id;

                for ($i = 1; $i < $commandDetail['params_number']; $i++) $data['data'][$key[$i]] = $params[$i];

                $post = json_encode($data, true);

                if($commandDetail['internal_module'] == 1)
                {
                    $request->merge(['data_valid' => $post]);

                    $response = Request::create($commandDetail['module'], $commandDetail['method']);

                    \Log::info("url response : " . Route::dispatch($response) );

                    goto log;
                }
                else
                {
                    if (!$dataLogged)
                    {
                        $this->telegram->sendMessage($chat_id, "Please login to access command " . $params[0]);
                        goto log;
                    }
                    else
                    {
                        for ($i = 1; $i < $commandDetail['params_number']; $i++) $query[$key[$i]] = $params[$i];

                        $requestModule = $this->client->request(
                            $commandDetail['method'],
                            $commandDetail['module'],
                            ['query' => $query]
                        );
                        $response      = json_decode($requestModule->getBody(), true);

                        if ($response['return_file'])
                        {
                            $this->telegram->sendDocument($chat_id, $response['file_url'], $response['message']);
                        }
                        else
                        {
                            $this->telegram->sendMessage($chat_id, $response['message']);
                        }

                        \Log::info("url response : " . $response['message']);

                        $success = 1;
                        goto log;
                    }
                }
            }
            else
            {
                $this->telegram->sendMessage($chat_id, "Command not found!");
                goto log;
            }

            log :
            $this->log->create(0, $this->lib->dateNow(), $chat_id, $dataLogged ? $dataLogged['bot_user_id'] : 0, 'Requested : ' . $request_bot, $success);
        }
    }

    public function coreAPI(Request $request)
    {
        $req = json_decode($request->getContent(), true);

        foreach ($this->botuser->all() as $key => $value)
        {
            if ($value['telegram_id'] != "76778490") // temporary
            {
                if ($req['return_file'])
                {
                    $this->telegram->sendDocument($value['telegram_id'], $req['file_url'], $req['message']);
                }
                else
                {
                    $this->telegram->sendMessage($value['telegram_id'], $req['message']);
                }
            }
        }
    }
}
