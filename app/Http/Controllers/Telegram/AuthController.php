<?php

namespace App\Http\Controllers\Telegram;

use App\Lib\Lib;
use App\Lib\TelegramLib;
use App\Repositories\BotUserRepository;
use App\BotUser;
use App\Repositories\SessionLoginRepository;
use App\SessionLogin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class AuthController extends Controller
{
    protected $botuser, $sessLog, $bot, $lib;

    public function __construct(BotUser $botuser, SessionLogin $sessionLogin)
    {
        $this->botuser  = new BotUserRepository($botuser);
        $this->sessLog  = new SessionLoginRepository($sessionLogin);
        $this->bot      = new TelegramLib();
        $this->lib      = new Lib();
    }

    public function register(Request $request)
    {
        try
        {
            $all        = isset($request->all()['data_valid']) ? json_decode($request->all()['data_valid'], true) : json_decode($request->getContent(), true);
            $data       = $all['data'];
            $request_by = $all['request_by'];

            if ($this->botuser->all()->where('telegram_id', $request_by)->count() != 0)
            {
                $message = "your telegram account is already registered.";
                goto response;
            }

            if ($this->botuser->all()->where('username', $data['username'])->count() != 0)
            {
                $message = "username ".$data['username']." is already exsist.";
                goto response;
            }

            if (strlen($data['password']) < 8)
            {
                $message = "password must be 8-16 characters";
                goto response;
            }

            $data = [
                'created_by'  => 0,
                'created_at'  => $this->lib->dateNow(),
                'updated_by'  => 0,
                'updated_at'  => $this->lib->dateNow(),
                'name'        => $data['name'],
                'username'    => $data['username'],
                'telegram_id' => $request_by,
                'password'    => Crypt::encrypt($data['password']),
                'id_code'     => Crypt::encrypt($data['username'].$this->lib->dateNow()),
                'status'      => 0
            ];

            $message = $this->botuser->create($data) ? "registration success" : "registration failed";

            response :
            $response = $this->lib->response(FALSE, $message, FALSE, FALSE, 200);
            $this->bot->sendMessage($request_by, $message);

            return $response;
        }
        catch (\Exception $e)
        {
            return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 500);
        }
    }

    public function login(Request $request)
    {
        try
        {
            $all        = isset($request->all()['data_valid']) ? json_decode($request->all()['data_valid'], true) : json_decode($request->getContent(), true);
            $data       = $all['data'];
            $request_by = $all['request_by'];

            if ($this->botuser->all()->firstWhere('username', $data['username']))
            {
                $dataUser      = $this->botuser->all()->firstWhere('username', $data['username']);
                $plainPassword = Crypt::decrypt($dataUser['password']);
                $dataLogged    = $this->sessLog->all()->where('bot_user_id', $dataUser['id'])->where('status', 1);

                if ($plainPassword != $data['password'])
                {
                    $message = "password incorrect";
                    goto response;
                }

                if ($dataLogged->count() != 0)
                {
                    $message = "you are already login";
                    goto response;
                }

                $dataLogin = [
                    'created_by'  => $dataUser['id'],
                    'created_at'  => $this->lib->dateNow(),
                    'bot_user_id' => $dataUser['id'],
                    'telegram_id' => $request_by,
                    'start'       => $this->lib->dateNow(),
                    'end'         => '',
                    'status'      => 1,
                ];

                $message = $this->sessLog->create($dataLogin) ? "login success" : "login failed";
                goto response;
            }
            else
            {
                $message = "can't found user " . $data['username'];
                goto response;
            }

            response :
            $response = $this->lib->response(FALSE, $message, FALSE, FALSE, 200);
            $this->bot->sendMessage($request_by, $message);

            return $response;
        }
        catch (\Exception $e)
        {
            return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 500);
        }
    }

    public function logout(Request $request)
    {
        try
        {
            $all        = isset($request->all()['data_valid']) ? json_decode($request->all()['data_valid'], true) : json_decode($request->getContent(), true);
            $request_by = $all['request_by'];
            $dataUser   = $this->botuser->all()->firstWhere('telegram_id', $request_by);

            if ($dataUser->count() != 0)
            {
                $dataLogin = $this->sessLog->all()->where('bot_user_id', $dataUser['id'])->where('status', 1)->last();

                if ($dataLogin['bot_user_id'] == $dataUser['id'])
                {
                    $dataLogout = [
                        'updated_by' => $dataUser['id'],
                        'updated_at' => $this->lib->dateNow(),
                        'end'        => $this->lib->dateNow(),
                        'status'     => 0
                    ];

                    $message = $this->sessLog->update($dataLogout, $dataLogin['id']) ? "logout success" : "logout failed";
                    goto response;
                }
                else
                {
                    $message = "You are not logged in";
                    goto response;
                }
            }
            else
            {
                $message = "can't found data";
                goto response;
            }

            response :
            $response = $this->lib->response(FALSE, $message, FALSE, FALSE, 200);
            $this->bot->sendMessage($request_by, $message);

            return $response;
        }
        catch (\Exception $e)
        {
            return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 500);
        }
    }
}
