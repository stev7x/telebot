<?php

namespace App\Http\Controllers;

use App\Cabang;
use App\Lib\Lib;
use App\Repositories\CabangRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class CabangController extends Controller
{
    protected $cabang, $lib;

    public function __construct(Cabang $cabang)
    {
        $this->cabang = new CabangRepository($cabang);
        $this->lib    = new Lib();
    }

    public function index()
    {
        try
        {
            $data['list'] = $this->cabang->all();

            return view('cabang/list', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function create()
    {
        try
        {
            return view('cabang/form');
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function store(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name'     => 'required',
                'host'     => 'required',
                'port'     => 'required',
                'sid'      => 'required',
                'username' => 'required',
                'password' => 'required',
                'status'   => 'required',
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            $data = [
                'created_by'  => Auth::id(),
                'created_at'  => $this->lib->dateNow(),
                'updated_by'  => Auth::id(),
                'updated_at'  => $this->lib->dateNow(),
                'nama_cabang' => $request['name'],
                'host'        => $request['host'],
                'port'        => $request['port'],
                'sid'         => $request['sid'],
                'username'    => $request['username'],
                'password'    => $request['password'],
                'status'      => $request['status']
            ];

            if ($response = $this->cabang->create($data))
            {
                return redirect('/cabang')->with('success', 'Cabang has been added');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function show($id)
    {
        try
        {
            $data['data'] = $this->cabang->show($id);

            return $data;
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function edit($id)
    {
        try
        {
            $data['data'] = $this->cabang->show($id);

            return view('cabang/form', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function update(Request $request, $id)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name'     => 'required',
                'host'     => 'required',
                'port'     => 'required',
                'sid'      => 'required',
                'username' => 'required',
                'password' => 'required',
                'status'   => 'required',
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            $data = [
                'updated_by'  => Auth::id(),
                'updated_at'  => $this->lib->dateNow(),
                'nama_cabang' => $request['name'],
                'host'        => $request['host'],
                'port'        => $request['port'],
                'sid'         => $request['sid'],
                'username'    => $request['username'],
                'password'    => $request['password'],
                'status'      => $request['status']
            ];

            if ($response = $this->cabang->update($data, $id))
            {
                return redirect('/cabang')->with('success', 'Cabang has been updated Successfully');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function destroy($id)
    {
        try
        {
            if ($response = $this->cabang->delete($id))
            {
                return redirect('/cabang')->with('success', 'Cabang has been deleted Successfully');
            }
            else
            {
                return redirect('/cabang')->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }
}
