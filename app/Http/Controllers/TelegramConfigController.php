<?php

namespace App\Http\Controllers;

use App\Lib\Lib;
use App\Lib\TelegramLib;
use Illuminate\Http\Request;

class TelegramConfigController extends Controller
{
    protected $telegram, $lib;

    public function __construct(TelegramLib $telegram, Lib $lib)
    {
        $this->telegram = new TelegramLib();
        $this->lib      = new Lib();
    }

    public function index()
    {
        try
        {
            $data['me']            = $this->telegram->bot();
            $data['token']         = $this->telegram->getToken();
            $webhookInfo           = json_decode($this->telegram->getWebhookInfo(), true);
            $data['status']        = $webhookInfo['result']['url'] != null ? 1 : 0;

            return view('config/config', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }

    public function bot_activate(Request $request)
    {
        $response = $request['status'] == 1 ? $this->telegram->setWebhook() : $this->telegram->deteleWebhook()->getDecodedBody()['description'];

        return $response;
    }

    public function change_token(Request $request)
    {
        $response = $this->telegram->setToken($request['new_token']) ? 'Success' : 'failed' ;

        return $response;
    }
}
