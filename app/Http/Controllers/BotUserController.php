<?php

namespace App\Http\Controllers;

use App\BotUser;
use App\Lib\Lib;
use App\Repositories\BotUserRepository;
use App\Repositories\RoleRepository;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;

class BotUserController extends Controller
{
    protected $botuser, $lib, $role;

    public function __construct(BotUser $botuser, Role $role)
    {
        $this->botuser = new BotUserRepository($botuser);
        $this->role    = new RoleRepository($role);
        $this->lib     = new Lib();
    }

    public function index()
    {
        try
        {
            $data['list'] = $this->botuser->all();
            $data['command'] = $this->role->all();

            return view('botUser/list', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }

    }

    public function create()
    {
        try
        {
            $data['role']['data']      = $this->role->all()->where('status', 1);
            $data['role']['countData'] = $this->role->all()->count();

            return view('botUser/form', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function store(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name'             => 'required',
                'username'         => 'required|unique:bot_user',
                'password'         => 'required',
                'confirm_password' => 'required',
                'role_id'          => 'required',
                'telegram_id'      => 'required',
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            if ($request['password'] != $request['confirm_password']) return redirect()->back()->withErrors("Confirm password is not valid")->withInput();

            if ($this->botuser->all()->where('telegram_id', $request['telegram_id'])->count() != 0) return redirect()->back()->withErrors("your telegram account is already registered")->withInput();

            if ($this->botuser->all()->where('username', $request['username'])->count() != 0) return redirect()->back()->withErrors("username " . $request['username'] . " is already exsist")->withInput();

            if (strlen($request['password']) < 8) return redirect()->back()->withErrors("password must be 8-16 characters")->withInput();

            $data = [
                'created_by'  => Auth::id(),
                'created_at'  => $this->lib->dateNow(),
                'updated_by'  => Auth::id(),
                'updated_at'  => $this->lib->dateNow(),
                'name'        => $request['name'],
                'username'    => $request['username'],
                'password'    => Crypt::encrypt($request['password']),
                'role_id'     => $request['role_id'],
                'telegram_id' => $request['telegram_id'],
                'status'      => 0
            ];

            if ($response = $this->botuser->create($data))
            {
                return redirect('/botuser')->with('success', 'Bot user has been added');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function show($id)
    {
        try
        {
            $command                 = $this->role->all();
            $data['data']            = $this->botuser->show($id);
            $data['data']['role_id'] = $command->firstWhere('id', $data['data']['role_id'])['name'];

            return $data;
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }

    }

    public function edit($id)
    {
        try
        {
            $data['data']              = $this->botuser->show($id);
            $data['role']['data']      = $this->role->all()->where('status', 1);
            $data['role']['countData'] = $this->role->all()->count();

            return view('botUser/form', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function update(Request $request, $id)
    {
        try
        {
            $plain = 0;

            if ($request['password'] == null && $request['confirm_password'] == null)
            {
                $plain                       = 1;
                $origin                      = $this->botuser->all()->firstWhere('id', $id);
                $request['password']         = $origin['password'];
                $request['confirm_password'] = $origin['password'];
            }

            $validator = Validator::make($request->all(), [
                'name'             => 'required',
                'username'         => 'required|unique:bot_user,username, ' .$id. ',id',
                'password'         => 'required',
                'confirm_password' => 'required',
                'status'           => 'required',
                'role_id'          => 'required',
                'telegram_id'      => 'required',
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            if ($request['password'] != $request['confirm_password']) return redirect()->back()->withErrors("Confirm password is not valid")->withInput();

            if (strlen($request['password']) < 8) return redirect()->back()->withErrors("password must be 8-16 characters")->withInput();

            $data = [
                'updated_by'  => Auth::id(),
                'updated_at'  => $this->lib->dateNow(),
                'name'        => $request['name'],
                'username'    => $request['username'],
                'password'    => $plain == 1 ? $request['password'] : Crypt::encrypt($request['password']),
                'role_id'     => $request['role_id'],
                'telegram_id' => $request['telegram_id'],
                'status'      => $request['status']
            ];

            if ($response = $this->botuser->update($data, $id))
            {
                return redirect('/botuser')->with('success', 'Bot user has been updated Successfully');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function destroy($id)
    {
        try
        {
            if ($response = $this->botuser->delete($id))
            {
                return redirect('/botuser')->with('success', 'Bot user has been deleted Successfully');
            }
            else
            {
                return redirect('/botuser')->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }
}
