<?php

namespace App\Http\Controllers;

use App\Command;
use App\Lib\Lib;
use App\Repositories\CommandRepository;
use App\Repositories\RoleRepository;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommandController extends Controller
{
    protected $command, $lib, $role;

    public function __construct(Command $command, Role $role)
    {
        $this->command = new CommandRepository($command);
        $this->role    = new RoleRepository($role);
        $this->lib     = new Lib();
    }

    public function index()
    {
        try
        {
            $data['list'] = $this->command->all();

            return view('command/list', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function create()
    {
        try
        {
            $data['role']['data']      = $this->role->all()->where('status', 1);
            $data['role']['countData'] = $this->role->all()->count();

            return view('command/form', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function store(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name'            => 'required',
                'command'         => 'required|unique:command',
                'description'     => 'required',
                'param'           => 'required',
                'internal_module' => 'required',
                'method'          => 'required',
                'role_id'         => 'required',
                'status'          => 'required',
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            if (($request['internal_module'] == 1) && (strpos($request['module'], '.') === true)) return redirect()->back()->withErrors("Input Route address  is not valid")->withInput();

            if (($request['internal_module'] == 0) && (strpos($request['module'], '/') === false)) return redirect()->back()->withErrors("Input URL address  is not valid")->withInput();

            $data = [
                'created_by'      => Auth::id(),
                'created_at'      => $this->lib->dateNow(),
                'updated_by'      => Auth::id(),
                'updated_at'      => $this->lib->dateNow(),
                'name'            => $request['name'],
                'description'     => $request['description'],
                'command'         => $request['command'],
                'param'           => $param = "command" . ($request['param'] !== null ? '#' : '') . $request['param'],
                'params_number'   => sizeof(explode('#', $param)),
                'internal_module' => $request['internal_module'],
                'module'          => $request['module'],
                'method'          => $request['method'],
                'role_id'         => json_encode($request['role_id'], true),
                'status'          => $request['status']
            ];

            if ($response = $this->command->create($data))
            {
                return redirect('/command')->with('success', 'Command has been added');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function show($id)
    {
        try
        {
            $data['data']          = $this->command->show($id);
            $data['data']['param'] = $data['data']['param'] == 'command' ? str_replace('command', $data['data']['command'], $data['data']['param']) : str_replace('command#', $data['data']['command'] . "#", $data['data']['param']);
            $dataRole              = json_decode($data['data']['role_id'], true);
            $role                  = $this->role->all();
            $parsedRole            = '';

            foreach ($dataRole as $key)
            {
                $parsedRole = $parsedRole . $role->firstWhere('id', $key)['name'] . ', ';
            }

            $data['data']['role_id'] = substr($parsedRole, 0, -2);

            return $data;
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function edit($id)
    {
        try
        {
            $data['data']              = $this->command->show($id);
            $data['data']['param']     = $data['data']['param'] == 'command' ? str_replace('command', '', $data['data']['param']) : str_replace('command#', '', $data['data']['param']);
            $data['data']['role_id']   = $data['data']['role_id'] == null ? [] : json_decode($data['data']['role_id'], true);
            $data['role']['data']      = $this->role->all()->where('status', 1);
            $data['role']['countData'] = $this->role->all()->count();

            return view('command/form', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function update(Request $request, $id)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name'            => 'required',
                'command'         => 'required|unique:command,command, ' .$id. ',id',
                'description'     => 'required',
                'internal_module' => 'required',
                'method'          => 'required',
                'role_id'         => 'required',
                'status'          => 'required'
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            $data = [
                'updated_by'      => Auth::id(),
                'updated_at'      => $this->lib->dateNow(),
                'name'            => $request['name'],
                'description'     => $request['description'],
                'command'         => $request['command'],
                'param'           => $param = "command" . ($request['param'] != null ? '#' : '') . $request['param'],
                'params_number'   => sizeof(explode('#', $param)),
                'internal_module' => $request['internal_module'],
                'module'          => $request['module'],
                'method'          => $request['method'],
                'role_id'         => json_encode($request['role_id'], true),
                'status'          => $request['status']
            ];

            if ($response = $this->command->update($data, $id))
            {
                return redirect('/command')->with('success', 'Command has been updated Successfully');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function destroy($id)
    {
        try
        {
            if ($response = $this->command->delete($id))
            {
                return redirect('/command')->with('success', 'Command has been deleted Successfully');
            }
            else
            {
                return redirect('/command')->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }
}
