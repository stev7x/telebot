<?php

namespace App\Http\Controllers;

use App\Lib\Lib;
use App\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;

class PanelUserController extends Controller
{
    protected $user, $lib;

    public function __construct(User $user)
    {
        $this->user = new UserRepository($user);
        $this->lib  = new Lib();
    }

    public function index()
    {
        try
        {
            $data['list'] = $this->user->all();

            return view('panelUser/list', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }

    }

    public function create()
    {
        try
        {
            return view('panelUser/form');
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function store(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name'             => 'required',
                'email'            => 'required|unique:users',
                'password'         => 'required',
                'confirm_password' => 'required',
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            if ($request['password'] != $request['confirm_password']) return redirect()->back()->withErrors("Confirm password is not valid")->withInput();

            if ($this->user->all()->where('email', $request['email'])->count() != 0) return redirect()->back()->withErrors("email ".$request['email']." is already exsist")->withInput();

            if (strlen($request['password']) < 8) return redirect()->back()->withErrors("password must be 8-16 characters")->withInput();

            $data = [
                'created_by' => Auth::id(),
                'created_at' => $this->lib->dateNow(),
                'updated_by' => Auth::id(),
                'updated_at' => $this->lib->dateNow(),
                'name'       => $request['name'],
                'email'      => $request['email'],
                'password'   => Crypt::encrypt($request['password'])
            ];

            if ($response = $this->user->create($data))
            {
                return redirect('/panel')->with('success', 'Panel user has been added');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function show($id)
    {
        try
        {
            $data['data'] = $this->user->show($id);

            return $data;
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }

    }

    public function edit($id)
    {
        try
        {
            $data['data'] = $this->user->show($id);

            return view('panelUser/form', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function update(Request $request, $id)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name'             => 'required',
                'username'         => 'required|unique:bot_user',
                'password'         => 'required',
                'confirm_password' => 'required',
                'role'             => 'required',
                'telegram_id'      => 'required',
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            $data = [
                'updated_by' => Auth::id(),
                'updated_at' => $this->lib->dateNow(),
                'name'       => $request['name'],
                'email'      => $request['email'],
                'password'   => Crypt::encrypt($request['password'])
            ];

            if ($response = $this->user->update($data, $id))
            {
                return redirect('/user')->with('success', 'Panel user has been updated Successfully');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function destroy($id)
    {
        try
        {
            if ($response = $this->user->delete($id))
            {
                return redirect('/user')->with('success', 'Bot user has been deleted Successfully');
            }
            else
            {
                return redirect('/user')->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }
}
