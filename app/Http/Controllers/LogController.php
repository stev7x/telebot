<?php

namespace App\Http\Controllers;

use App\BotUser;
use App\Log;
use App\Repositories\BotUserRepository;
use App\Repositories\LogRepository;
use Illuminate\Http\Request;

class LogController extends Controller
{
    protected $log, $botuser, $now;

    public function __construct(Log $log, BotUser $botuser)
    {
        $this->log     = new LogRepository($log);
        $this->botuser = new BotUserRepository($botuser);
    }

    public function index()
    {
        try
        {
            $data['list'] = $this->log->all()->sortByDesc('id');
            $user         = $this->botuser->all();

            foreach ($user as $value)
            {
                $data['botuser'][$value['id']] = $value;
            }

            return view('log/list', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }
}
