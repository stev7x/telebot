<?php

namespace App\Http\Controllers;

use App\BotUser;
use App\Lib\Lib;
use App\Repositories\BotUserRepository;
use App\Repositories\RoleRepository;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected $role, $lib;

    public function __construct(Role $role)
    {
        $this->role    = new RoleRepository($role);
        $this->lib     = new Lib();
    }

    public function index()
    {
        try
        {
            $data['list'] = $this->role->all();

            return view('role/list', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function create()
    {
        try
        {
            return view('role/form');
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function store(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            $data = [
                'created_by'  => Auth::id(),
                'created_at'  => $this->lib->dateNow(),
                'updated_by'  => Auth::id(),
                'updated_at'  => $this->lib->dateNow(),
                'name'        => $request['name'],
                'status'      => $request['status']
            ];

            if ($response = $this->role->create($data))
            {
                return redirect('/role')->with('success', 'Bot user role has been added');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function show($id)
    {
        try
        {
            $data['data'] = $this->role->show($id);

            return $data;
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function edit($id)
    {
        try
        {
            $data['data'] = $this->role->show($id);

            return view('role/form', $data);
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function update(Request $request, $id)
    {
        try
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

            $data = [
                'updated_by'  => Auth::id(),
                'updated_at'  => $this->lib->dateNow(),
                'name'        => $request['name'],
                'status'      => $request['status']
            ];

            if ($response = $this->role->update($data, $id))
            {
                return redirect('/role')->with('success', 'Bot user role has been updated Successfully');
            }
            else
            {
                return redirect()->back()->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }

    public function destroy($id)
    {
        try
        {
            if ($response = $this->role->delete($id))
            {
                return redirect('/role')->with('success', 'Bot user role has been deleted Successfully');
            }
            else
            {
                return redirect('/role')->withErrors($response)->withInput();
            }
        }
        catch (\Exception $exception)
        {
            report($exception);

            return false;
        }
    }
}
