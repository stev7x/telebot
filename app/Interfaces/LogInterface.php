<?php

namespace App\Interfaces;

interface LogInterface
{
    public function all();

    public function create($created_by, $created_at, $telegram_id, $bot_user_id, $activity, $success);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);

    public function where($param);
}